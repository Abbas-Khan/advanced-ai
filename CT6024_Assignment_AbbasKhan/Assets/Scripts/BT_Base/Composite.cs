﻿///////////////////////////////////////////////////////////////////
// Name: Composite.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used as the base of the BT system
///////////////////////////////////////////////////////////////////

// Using calls
using System.Collections.Generic;

public class Composite : Node
{
    // Protectd integer used to hold the current index position in the node list
    protected int iCurrentIndex = 0;
    // List to hold all the nodes of a system
    private List<Node> xChildNodes;

    // Initialise function used to setup the variables
    protected void Init()
    {
        // Set the index to 0
        iCurrentIndex = 0;
        // Create a node list
        xChildNodes = new List<Node>();
    }

    // Add child function used to add nodes to the node list
    public void AddChild(Node a_xNewChild)
    {
        // Add the passed in node to the list
        xChildNodes.Add(a_xNewChild);
    }

    // Get Child function used to get the node list
    public List<Node> GetChildBehaviours()
    {
        return xChildNodes;
    }

    // Reset function used to reset the index
    protected void Reset()
    {
        iCurrentIndex = 0;
    }
}
