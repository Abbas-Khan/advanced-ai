﻿///////////////////////////////////////////////////////////////////
// Name: Agent.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used to manage agents
///////////////////////////////////////////////////////////////////

// Using calls
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Agent : MonoBehaviour
{
    // Integer to hold the alertness level of an agent
    private int iAlertLevel = 0;
    // Enum to determine which BT to use
    private enum BT_Type
    {
        BTGuard,
        BTGuard_Patrol,
        BTGuard_VisionClose,
        BTGuard_VisionRanged,
        BTGuard_LKP,
        BTGuard_Knowledge,
        BTGuard_Chase,
    };
    [SerializeField] private BT_Type eBT_Type;
    // Behaviour Trees
    private BTGuard xBTGuard;
    private BTGuard_Knowledge xBTGuard_Knowledge;
    private BTGuard_LKP xBTGuard_LKP;
    private BTGuard_Patrol xBTGuard_Patrol;
    private BTGuard_VisionClose xBTGuard_VisionClose;
    private BTGuard_VisionRanged xBTGuard_VisionRanged;
    private BTGuard_Chase xBTGuard_Chase;
    // GameObject references to the patrol points
    [SerializeField] private GameObject[] xPatrolPoints;
    // Vector3 to hold the target position
    private Vector3 v3TargetPosition = Vector3.zero;
    // NavMeshAgent to hold this agent's NavMeshAgent component
    private NavMeshAgent xNavAgent;
    // Renderer to hold this agent's renderer component
    private Renderer xRenderer;
    // Float to hold amount of time for the search action to be performed
    [SerializeField] private float fSearchTime = 15;
    // GameObject reference to the target 
    [SerializeField] private GameObject xTarget;
    // Vector3 to hold the last known position for this agent
    private Vector3 v3LKP;

    // Utilty function used to show debug info
    private void OnDrawGizmosSelected()
    {
        // Only show the gizmo for certain types
        if(eBT_Type == BT_Type.BTGuard_VisionClose || eBT_Type == BT_Type.BTGuard)
        {
            // Close detection ring visualisation
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, 2f);
        }
        // Only show the gizmo for certain types
        if (eBT_Type == BT_Type.BTGuard_LKP || eBT_Type == BT_Type.BTGuard)
        {
            // LKP area visualisation
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(v3LKP, 5f);
            // Vision detection ring when not Alert Level 0
            Gizmos.DrawWireSphere(transform.position, 10f);
        }
        // Only show the gizmo for certain types
        if (eBT_Type == BT_Type.BTGuard_Knowledge || eBT_Type == BT_Type.BTGuard)
        {
            // Knowledge Propegation radius visualisation
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, 10.1f);
        }
        // Only show the gizmo for certain types
        if (eBT_Type == BT_Type.BTGuard_VisionRanged || eBT_Type == BT_Type.BTGuard)
        {
            // Ranged vision detection visualisation
            Gizmos.color = Color.magenta;
            // Vision detection ring when Alert Level 0
            Gizmos.DrawWireSphere(transform.position, 7f);
            // Rays for the vision radius and max distance
            Gizmos.DrawRay(transform.position, transform.forward.normalized * 20);
            Gizmos.DrawRay(transform.position, (transform.forward + transform.right).normalized * 20);
            Gizmos.DrawRay(transform.position, (transform.forward - transform.right).normalized * 20);
        }
        // Only show the gizmo for certain types
        if (eBT_Type == BT_Type.BTGuard_Patrol || eBT_Type == BT_Type.BTGuard)
        {
            Gizmos.color = Color.red;
            // Rays for the patrol point
            for (int i = 0; i < xPatrolPoints.Length - 1; i++)
            {
                Gizmos.DrawRay(xPatrolPoints[i].transform.position, xPatrolPoints[i + 1].transform.position - xPatrolPoints[i].transform.position);
            }
        }
        // Only show the gizmo for certain types
        if (eBT_Type == BT_Type.BTGuard_Chase)
        {
            Gizmos.color = Color.red;
            // Rays for the vision radius and max distance
            Gizmos.DrawRay(transform.position, transform.forward.normalized * 20);
            Gizmos.DrawRay(transform.position, (transform.forward + transform.right).normalized * 20);
            Gizmos.DrawRay(transform.position, (transform.forward - transform.right).normalized * 20);

        }
    }
    
    // Start runs on startup
    void Start()
    {
        // Get the components from the local gameobject 
        xNavAgent = GetComponent<NavMeshAgent>();
        xRenderer = GetComponent<Renderer>();
        // Create a new BT for this agent
        switch(eBT_Type)
        {
            case BT_Type.BTGuard:
                {
                    xBTGuard = new BTGuard(this);
                    break;
                }
            case BT_Type.BTGuard_Knowledge:
                {
                    xBTGuard_Knowledge = new BTGuard_Knowledge(this);
                    break;
                }
            case BT_Type.BTGuard_LKP:
                {
                    xBTGuard_LKP = new BTGuard_LKP(this);
                    break;
                }
            case BT_Type.BTGuard_Patrol:
                {
                    xBTGuard_Patrol = new BTGuard_Patrol(this);
                    break;
                }
            case BT_Type.BTGuard_VisionClose:
                {
                    xBTGuard_VisionClose = new BTGuard_VisionClose(this);
                    break;
                }
            case BT_Type.BTGuard_VisionRanged:
                {
                    xBTGuard_VisionRanged = new BTGuard_VisionRanged(this);
                    break;
                }
            case BT_Type.BTGuard_Chase:
                {
                    xBTGuard_Chase = new BTGuard_Chase(this);
                    break;
                }
        }
    }

    // Update runs once per frame
    void Update()
    {
        // Update the BT
        switch (eBT_Type)
        {
            case BT_Type.BTGuard:
                {
                    xBTGuard.Update();
                    break;
                }
            case BT_Type.BTGuard_Knowledge:
                {
                    xBTGuard_Knowledge.Update();
                    break;
                }
            case BT_Type.BTGuard_LKP:
                {
                    xBTGuard_LKP.Update();
                    break;
                }
            case BT_Type.BTGuard_Patrol:
                {
                    xBTGuard_Patrol.Update();
                    break;
                }
            case BT_Type.BTGuard_VisionClose:
                {
                    xBTGuard_VisionClose.Update();
                    break;
                }
            case BT_Type.BTGuard_VisionRanged:
                {
                    xBTGuard_VisionRanged.Update();
                    break;
                }
            case BT_Type.BTGuard_Chase:
                {
                    xBTGuard_Chase.Update();
                    break;
                }
        }
    }

    // Get and Set fuctions for the colour of the AI
    public Color GetColor()
    {
        return xRenderer.material.color;
    }
    public void SetColour(Color a_xNewColour)
    {
        // Set the renderer's colour to the new colour
        xRenderer.material.color = a_xNewColour;
    }

    // Get and Set functions for the Alert Level
    public int GetAlertLevel()
    {
        return iAlertLevel;
    }
    public void SetAlertLevel(int a_iAlertLevel)
    {
        // Set the alert level to the new alert level
        iAlertLevel = a_iAlertLevel;
    }

    // Get function for the search time
    public float GetSearchTime()
    {
        return fSearchTime;
    }

    // Get function for the patrol points of the object
    public GameObject[] GetPatrolPoints()
    {
        return xPatrolPoints;
    }

    // Get function for the target object
    public GameObject GetTargetObject()
    {
        return xTarget;
    }

    // Get function for the transform of the object
    public Transform GetTransform()
    {
        return gameObject.transform;
    }

    // Update LKP reciever function to collect information from the Knowledge and Event Manager broadcast
    private void UpdateLKP(Vector3 v3NewLKP)
    {
        SetLKP(v3NewLKP);
    }
    // Get and Set functions for the LKP
    public Vector3 GetLKP()
    {
        return v3LKP;
    }
    public void SetLKP(Vector3 v3NewLKP)
    {
        // Set the LKP to the new LKP
        v3LKP = v3NewLKP;
    }

    // Get and Set functions for the destination positions
    public Vector3 GetTargetPosition()
    {
        return v3TargetPosition;
    }
    public void SetTargetPosition(Vector3 TargetPos)
    {
        v3TargetPosition = TargetPos;
    }

    // Get function for the NavMeshAget of the object
    public NavMeshAgent GetNavMeshAgent()
    {
        return xNavAgent;
    }

    // function to call the Search Cooldown IEnumerator
    public void SerachCooldownCall()
    {
        StartCoroutine(SearchCooldown());
    }
    private IEnumerator SearchCooldown()
    {
        yield return new WaitForSeconds(fSearchTime);
        // Reset the Alert Level and LKP
        SetAlertLevel(0);
        SetLKP(Vector3.zero);
    }
}