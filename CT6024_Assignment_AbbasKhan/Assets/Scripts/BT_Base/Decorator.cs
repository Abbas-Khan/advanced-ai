﻿///////////////////////////////////////////////////////////////////
// Name: Decorator.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used as the base of the BT system
///////////////////////////////////////////////////////////////////

public class Decorator : Node
{
    // Private variable to hold the child node
    private Node xChildNode;

    // Constrctor
    public Decorator(Node a_xChildnode)
    {
        // Assign the values
        this.xChildNode = a_xChildnode;
    }

    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        return BEHAVIOUR_STATUS.NONE;
    }

    // Get function used to get the child node
    public Node GetChild()
    {
        return xChildNode;
    }
}
