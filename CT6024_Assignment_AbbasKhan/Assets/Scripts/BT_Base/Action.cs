﻿///////////////////////////////////////////////////////////////////
// Name: Action.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used as the base of the BT system
///////////////////////////////////////////////////////////////////

public class Action : Node
{
    // Agent variable used to hold the owner agent
    private Agent xOwnerAgent;

    // Constructor
    public Action(Agent a_xOwnerAgent)
    {
        // Assign the values
        xOwnerAgent = a_xOwnerAgent;
    }

    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        return BEHAVIOUR_STATUS.NONE;
    }

    // Function used to retrieve the owner agent
    public Agent GetOwner()
    {
        return xOwnerAgent;
    }
}
