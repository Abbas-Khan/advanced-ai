﻿///////////////////////////////////////////////////////////////////
// Name: Node.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used as the base of the BT system
///////////////////////////////////////////////////////////////////

// Enumeration used to hold the states of the behaviour
public enum BEHAVIOUR_STATUS
{
    SUCCESS,
    FAILURE,
    RUNNING,
    NONE
}

public class Node
{
    // Hide the warning as it only states that the variable value is assigned but not directly used in this class
    #pragma warning disable 0414
    // Private enum used to store the behaviour status
    private BEHAVIOUR_STATUS Behaviour_Status;
    #pragma warning restore 0414

    // Constructor
    public Node ()
    {
        // Assign the values
        Behaviour_Status = BEHAVIOUR_STATUS.NONE;
    }

    // Virtual Update loop for this script
    public virtual BEHAVIOUR_STATUS Update()
    {
        return BEHAVIOUR_STATUS.NONE;
    }

}
