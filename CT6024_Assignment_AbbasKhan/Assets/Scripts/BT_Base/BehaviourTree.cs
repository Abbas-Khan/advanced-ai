﻿///////////////////////////////////////////////////////////////////
// Name: BehaviourTree.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used as the base of the BT system
///////////////////////////////////////////////////////////////////

public class BehaviourTree
{
    // Agent variable used to hold the owner agent
    private Agent xOwnerAgent;

    // Constructor
    public BehaviourTree(Agent a_xOwnerAgent)
    {
        // Assign the values
        xOwnerAgent = a_xOwnerAgent;
    }

    // Virtual Update loop for this script
    public virtual void Update() { }

    // Function used to retrieve the owner agent
    public Agent GetOwner()
    {
        return xOwnerAgent;
    }
}