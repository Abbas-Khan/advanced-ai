﻿///////////////////////////////////////////////////////////////////
// Name: SpyUndetected.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used to poll a condition inherits from condition
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;

public class SpyUndetected : Condition
{
    // Layer mask variables used to optimise the searching
    private LayerMask xEnemyLayer;
    private LayerMask xFriendlyLayer;
    // Float variables used to determine the ranges of the system
    private float fAlertRange = 10f;
    private float fProximityRange = 2f;
    private float fVisionRange = 20f;
    private float fVisionLowThresholdRange = 7f;
    private float fDetectionAngle = 45f;

    // Constructor
    public SpyUndetected(Agent a_xOwnerAgent, string a_sEnemyLayerName, string a_sFriendlyLayerName, float a_fAlertRange, float a_fProximityRange, float a_fVisionRange, float a_fVisionThreshold, float a_fDetectionAngle) : base(a_xOwnerAgent)
    {
        // Assign the values
        xEnemyLayer = LayerMask.GetMask(a_sEnemyLayerName);
        xFriendlyLayer = LayerMask.GetMask(a_sFriendlyLayerName);
        fAlertRange = a_fAlertRange;
        fProximityRange = a_fProximityRange;
        fVisionRange = a_fVisionRange;
        fVisionLowThresholdRange = a_fVisionThreshold;
        fDetectionAngle = a_fDetectionAngle;
    }

    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        // Check if the target is detected by being too close to the enemy or by vision
        if (CheckDetectionByProximity() == true || CheckDetectionByVision() == true)
        {
            // If the current agent's alert level is 10
            if (GetOwner().GetAlertLevel() == 10)
            {
                // Alert nearby allies
                AlertNearbyAllies();
            }
            // Return failure as the Spy was detected
            return BEHAVIOUR_STATUS.FAILURE;
        }
        // Otherwise
        else if (CheckDetectionByProximity() == false && CheckDetectionByVision() == false)
        {
            // Return success as the spy was not detected
            return BEHAVIOUR_STATUS.SUCCESS;
        }
        // Default to returning success
        return BEHAVIOUR_STATUS.SUCCESS;
    }

    // Function to alert nearby enemies within a range
    void AlertNearbyAllies()
    {
        // Array of colliders that is filled with the objects found by the overlap sphere on the target layer
        Collider[] xHitColliders = Physics.OverlapSphere(GetOwner().transform.position, fAlertRange, xFriendlyLayer);
        // If the array is bigger than 0
        if (xHitColliders.Length > 0)
        {
            // Loop through the amount of objects in the array
            for (int i = 0; i < xHitColliders.Length; i++)
            {
                // Extract the agent component
                Agent xAgentComponent = xHitColliders[i].GetComponent<Agent>();
                // Null check and not self check
                if (xAgentComponent != null && xAgentComponent != GetOwner())
                {
                    // Set the LKP and alerlevel
                    xAgentComponent.SetLKP(GetOwner().GetLKP());
                    xAgentComponent.SetAlertLevel(10);
                }
            }
        }
    }

    // Boolean for checking if the target is close
    bool CheckDetectionByProximity()
    {
        // Array of colliders that is filled with the objects found by the overlap sphere on the target layer
        Collider[] xHitColliders = Physics.OverlapSphere(GetOwner().transform.position, fProximityRange, xEnemyLayer);
        // If the array is bigger than 0
        if (xHitColliders.Length > 0)
        {
            // Set the alert level and the LKP
            GetOwner().SetAlertLevel(10);
            GetOwner().SetLKP(xHitColliders[0].gameObject.transform.position);
            // Return true
            return true;
        }
        else
        {
            // Return false
            return false;
        }
    }

    // Boolean for checking if the target is seen
    bool CheckDetectionByVision()
    {
        // Create a raycast hit
        RaycastHit xHit;
        // Directions for the target and the ray
        Vector3 v3RayDirection = GetOwner().GetTargetObject().transform.position - GetOwner().GetTransform().position;
        Vector3 v3TargetDirection = (GetOwner().GetTransform().forward.normalized * fVisionRange);

        // Angle is equal to the distance between the target direction and the ray direction
        float fAngle = Vector3.Angle(v3TargetDirection, v3RayDirection);
        // If the raycast hits anything
        if (Physics.Raycast(GetOwner().GetTransform().position, v3RayDirection, out xHit, fVisionRange))
        {
            // If it's position is equal to the target's position, it must be the target
            // and if the angle is less than 45
            if (xHit.transform == GetOwner().GetTargetObject().transform && (fAngle < fDetectionAngle))
            {
                // Set the LKP
                GetOwner().SetLKP(xHit.transform.position);
                // If the distance is lower than the threshold
                if (xHit.distance < fVisionLowThresholdRange)
                {
                    // Set the alert level
                    GetOwner().SetAlertLevel(10);
                }
                // Otherwise
                else if (xHit.distance < fVisionRange)
                {
                    // Gradually increase the alert level
                    GetOwner().SetAlertLevel(GetOwner().GetAlertLevel() + 1);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }
}