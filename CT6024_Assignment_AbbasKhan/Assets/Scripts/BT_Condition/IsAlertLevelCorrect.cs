﻿///////////////////////////////////////////////////////////////////
// Name: IsAlertLevelCorrect.cs
// Author: Abbas Khan
// Date: 08/10/18
// Bio: Used to poll a condition inherits from condition
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;

public class IsAlertLevelCorrect : Condition
{
    // Integer used to hold the target alertness level for this node (0,5 or 10)
    private int iTargetAlertLevel = 0;

    // Constructor
    public IsAlertLevelCorrect(Agent a_xOwnerAgent, int a_iTargetValue) : base(a_xOwnerAgent)
    {
        // Assign the values
        iTargetAlertLevel = a_iTargetValue;
    }

    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        // If the owner is at alert level 0
        if (GetOwner().GetAlertLevel() == 0 && GetOwner().GetNavMeshAgent().isStopped == false)
        {
            // Set the colour to black to indicate this
            GetOwner().SetColour(Color.black);
            // Stop any running coroutine for this agent
            GetOwner().StopAllCoroutines();
        }
        // If the owner's alert level is between 1 to 9
        else if (GetOwner().GetAlertLevel() > 0 && GetOwner().GetAlertLevel() < 10)
        {
            // Set the colour to white to indicate this
            GetOwner().SetColour(Color.white);
            // Start the search coroutine timer
            GetOwner().SerachCooldownCall();
        }
        // If the owner is at alert level 10
        else if (GetOwner().GetAlertLevel() == 10)
        {
            // Set the colour to red to indicate this
            GetOwner().SetColour(Color.red);
            // Stop any running coroutine for this agent
            GetOwner().StopAllCoroutines();
        }

        // if the target level is 0
        if (iTargetAlertLevel == 0)
        {
            // If the current alert level is 0
            if (GetOwner().GetAlertLevel() == 0)
            {
                // Return success as they are both the same
                return BEHAVIOUR_STATUS.SUCCESS;
            }
            else
            {
                // Return failure
                return BEHAVIOUR_STATUS.FAILURE;
            }
        }
        // If the target alert level is 5
        if (iTargetAlertLevel == 5)
        {
            // If the curremt alert level is within bounds
            if (GetOwner().GetAlertLevel() > 0 && GetOwner().GetAlertLevel() < 10)
            {
                // Return success as they are within the bounds
                return BEHAVIOUR_STATUS.SUCCESS;
            }
            else
            {
                // Return failure
                return BEHAVIOUR_STATUS.FAILURE;
            }
        }
        // If the target alert level is 10
        if (iTargetAlertLevel == 10)
        {
            // If the current alert level is 10
            if (GetOwner().GetAlertLevel() == 10)
            {
                // Return success as they are both the same
                return BEHAVIOUR_STATUS.SUCCESS;
            }
            else
            {
                // Return failure
                return BEHAVIOUR_STATUS.FAILURE;
            }
        }
        // Default to failure
        return BEHAVIOUR_STATUS.FAILURE;
    }
}