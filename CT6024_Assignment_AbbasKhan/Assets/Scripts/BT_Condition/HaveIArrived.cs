﻿///////////////////////////////////////////////////////////////////
// Name: HaveIArrived.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used to poll a condition inherits from condition
///////////////////////////////////////////////////////////////////

public class HaveIArrived : Condition
{
    // Float used to hold the acceptable arrival range
    private float fArrivalRange = 1.0f;

    // Constructor
    public HaveIArrived(Agent a_xOwnerAgent, float a_fArrivalRange) : base(a_xOwnerAgent)
    {
        // Assign the values
        fArrivalRange = a_fArrivalRange;
    }

    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        // If the remaining distance is within acceptable range
        if (GetOwner().GetNavMeshAgent().remainingDistance < fArrivalRange)
        {
            // Return success
            return BEHAVIOUR_STATUS.SUCCESS;
        }
        else
        {
            // Return failure otherwise
            return BEHAVIOUR_STATUS.FAILURE;
        }
    }
}