﻿///////////////////////////////////////////////////////////////////
// Name: BTGuard_Knowledge.cs
// Author: Abbas Khan
// Date: 16/12/18
// Bio: Used to control the guard agents inherits from behaviourtree
///////////////////////////////////////////////////////////////////

public class BTGuard_Knowledge : BehaviourTree
{
    // Overall Tree
    private Selector xRootSelector;
    private Sequence xPatrolSequence;
    private Selector xSpyDetectionSelector;
    private Sequence xSearchSequence;
    private Sequence xChaseSequence;

    // Patrol Sequence
    private IsAlertLevelCorrect xAlertLevel_Patrol;
    private SpyUndetected xSpyUndetected_Patrol;
    private HaveIArrived xHaveIArrived_Patrol;
    private FindNextPatrolPoint xFindTarget_Patrol;
    private MoveToTarget xMoveToTarget_Patrol;

    // Chase Sequence
    private IsAlertLevelCorrect xAlertLevel_Chase;
    private SpyDetected xSpyDetected_Chase;
    private FindSpyPosition xFindTarget_Chase;
    private MoveToTarget xMoveToTarget_Chase;

    // Search Sequence
    private IsAlertLevelCorrect xAlertLevel_Search;
    private SpyUndetected xSpyUndetected_Search;
    private HaveIArrived xHaveIArrived_Search;
    private FindRandomPosition xFindRandomPosition_Search;
    private MoveToTarget xMoveToTarget_Search;

    public BTGuard_Knowledge(Agent a_xOwnerAgent) : base(a_xOwnerAgent)
    {
        // Setup composites
        xRootSelector = new Selector();
        xPatrolSequence = new Sequence();
        xSpyDetectionSelector = new Selector();
        xChaseSequence = new Sequence();
        xSearchSequence = new Sequence();

        // Setup patrol sequence
        xAlertLevel_Patrol = new IsAlertLevelCorrect(GetOwner(), 0);
        xSpyUndetected_Patrol = new SpyUndetected(GetOwner(), "Player", "Guard", 10f, 2f, 20f, 7f, 45f);
        xHaveIArrived_Patrol = new HaveIArrived(GetOwner(), 1f);
        xFindTarget_Patrol = new FindNextPatrolPoint(GetOwner(), 1f);
        xMoveToTarget_Patrol = new MoveToTarget(GetOwner());

        // Setup chase sequence
        xAlertLevel_Chase = new IsAlertLevelCorrect(GetOwner(), 10);
        xSpyDetected_Chase = new SpyDetected(GetOwner(), "Player", "Guard", 10f, 2f, 20f, 10f, 45f);
        xFindTarget_Chase = new FindSpyPosition(GetOwner());
        xMoveToTarget_Chase = new MoveToTarget(GetOwner());

        // Setup search sequence
        xAlertLevel_Search = new IsAlertLevelCorrect(GetOwner(), 5);
        xSpyUndetected_Search = new SpyUndetected(GetOwner(), "Player", "Guard", 10f, 2f, 20f, 7f, 45f);
        xHaveIArrived_Search = new HaveIArrived(GetOwner(), 1f);
        xFindRandomPosition_Search = new FindRandomPosition(GetOwner(), 5f);
        xMoveToTarget_Search = new MoveToTarget(GetOwner());

        // Link the Tree
        xRootSelector.AddChild(xPatrolSequence);
        xRootSelector.AddChild(xSpyDetectionSelector);

        xPatrolSequence.AddChild(xAlertLevel_Patrol);
        xPatrolSequence.AddChild(xSpyUndetected_Patrol);
        xPatrolSequence.AddChild(xHaveIArrived_Patrol);
        xPatrolSequence.AddChild(xFindTarget_Patrol);
        xPatrolSequence.AddChild(xMoveToTarget_Patrol);

        xSpyDetectionSelector.AddChild(xChaseSequence);
        xSpyDetectionSelector.AddChild(xSearchSequence);

        xChaseSequence.AddChild(xAlertLevel_Chase);
        xChaseSequence.AddChild(xSpyDetected_Chase);
        xChaseSequence.AddChild(xFindTarget_Chase);
        xChaseSequence.AddChild(xMoveToTarget_Chase);

        xSearchSequence.AddChild(xAlertLevel_Search);
        xSearchSequence.AddChild(xSpyUndetected_Search);
        xSearchSequence.AddChild(xHaveIArrived_Search);
        xSearchSequence.AddChild(xFindRandomPosition_Search);
        xSearchSequence.AddChild(xMoveToTarget_Search);
    }

    public override void Update()
    {
        xRootSelector.Update();
    }
}