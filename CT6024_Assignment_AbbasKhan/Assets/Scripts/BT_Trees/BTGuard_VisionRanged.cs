﻿///////////////////////////////////////////////////////////////////
// Name: BTGuard_VisionRanged.cs
// Author: Abbas Khan
// Date: 16/12/18
// Bio: Used to control the guard agents inherits from behaviourtree
///////////////////////////////////////////////////////////////////

public class BTGuard_VisionRanged : BehaviourTree
{
    // Overall Tree
    private Selector xRootSelector;
    private Sequence xPatrolSequence;
    private Selector xSpyDetectionSelector;
    private Sequence xSearchSequence;
    private Sequence xChaseSequence;

    // Patrol Sequence
    private IsAlertLevelCorrect xAlertLevel_Patrol;
    private SpyUndetected xSpyUndetected_Patrol;

    // Chase Sequence
    private IsAlertLevelCorrect xAlertLevel_Chase;
    private SpyDetected xSpyDetected_Chase;

    // Search Sequence
    private IsAlertLevelCorrect xAlertLevel_Search;
    private SpyUndetected xSpyUndetected_Search;

    public BTGuard_VisionRanged(Agent a_xOwnerAgent) : base(a_xOwnerAgent)
    {
        // Setup composites
        xRootSelector = new Selector();
        xPatrolSequence = new Sequence();
        xSpyDetectionSelector = new Selector();
        xChaseSequence = new Sequence();
        xSearchSequence = new Sequence();

        // Setup patrol sequence
        xAlertLevel_Patrol = new IsAlertLevelCorrect(GetOwner(), 0);
        xSpyUndetected_Patrol = new SpyUndetected(GetOwner(), "Player", "Guard", 10f, 2f, 20f, 7f, 45f);

        // Setup chase sequence
        xAlertLevel_Chase = new IsAlertLevelCorrect(GetOwner(), 10);
        xSpyDetected_Chase = new SpyDetected(GetOwner(), "Player", "Guard", 10f, 2f, 20f, 10f, 45f);

        // Setup search sequence
        xAlertLevel_Search = new IsAlertLevelCorrect(GetOwner(), 5);
        xSpyUndetected_Search = new SpyUndetected(GetOwner(), "Player", "Guard", 10f, 2f, 20f, 7f, 45f);

        // Link the Tree
        xRootSelector.AddChild(xPatrolSequence);
        xRootSelector.AddChild(xSpyDetectionSelector);

        xPatrolSequence.AddChild(xAlertLevel_Patrol);
        xPatrolSequence.AddChild(xSpyUndetected_Patrol);

        xSpyDetectionSelector.AddChild(xChaseSequence);
        xSpyDetectionSelector.AddChild(xSearchSequence);

        xChaseSequence.AddChild(xAlertLevel_Chase);
        xChaseSequence.AddChild(xSpyDetected_Chase);

        xSearchSequence.AddChild(xAlertLevel_Search);
        xSearchSequence.AddChild(xSpyUndetected_Search);
    }

    public override void Update()
    {
        xRootSelector.Update();
    }
}