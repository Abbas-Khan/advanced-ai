﻿///////////////////////////////////////////////////////////////////
// Name: BTGuard_Patrol.cs
// Author: Abbas Khan
// Date: 16/12/18
// Bio: Used to control the guard agents inherits from behaviourtree
///////////////////////////////////////////////////////////////////

public class BTGuard_Patrol : BehaviourTree
{
    // Overall Tree
    private Selector xRootSelector;
    private Sequence xPatrolSequence;

    // Patrol Sequence
    private HaveIArrived xHaveIArrived_Patrol;
    private Wait xWait_Patrol;
    private FindNextPatrolPoint xFindTarget_Patrol;
    private MoveToTarget xMoveToTarget_Patrol;

    public BTGuard_Patrol(Agent a_xOwnerAgent) : base(a_xOwnerAgent)
    {
        // Setup composites
        xRootSelector = new Selector();
        xPatrolSequence = new Sequence();

        // Setup patrol sequence
        xHaveIArrived_Patrol = new HaveIArrived(GetOwner(), 1f);
        xWait_Patrol = new Wait(GetOwner(), 0.5f);
        xFindTarget_Patrol = new FindNextPatrolPoint(GetOwner(), 1f);
        xMoveToTarget_Patrol = new MoveToTarget(GetOwner());

        // Link the Tree
        xRootSelector.AddChild(xPatrolSequence);

        xPatrolSequence.AddChild(xHaveIArrived_Patrol);
        xPatrolSequence.AddChild(xWait_Patrol);
        xPatrolSequence.AddChild(xFindTarget_Patrol);
        xPatrolSequence.AddChild(xMoveToTarget_Patrol);
    }

    public override void Update()
    {
        xRootSelector.Update();
    }
}