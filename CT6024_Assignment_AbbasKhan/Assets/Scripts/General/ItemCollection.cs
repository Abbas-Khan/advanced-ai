﻿///////////////////////////////////////////////////////////////////
// Name: ItemCollection.cs
// Author: Abbas Khan
// Date: 07/10/18
// Bio: Used to collect the item
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;

public class ItemCollection : MonoBehaviour
{
    // Gameobject to hold the door object
    [SerializeField] private GameObject xDoor;
    // Function that occurs on trigger enter
    private void OnTriggerEnter(Collider a_xOther)
    {
        // If the object is the spy
        if (a_xOther.name == "Spy")
        {
            // Destroy this object and the door object to allow for passage
            Destroy(xDoor);
            Destroy(gameObject);
        }
    }
}
