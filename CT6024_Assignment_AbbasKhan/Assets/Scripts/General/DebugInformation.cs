﻿///////////////////////////////////////////////////////////////////
// Name: DebugInformation.cs
// Author: Abbas Khan
// Date: 08/12/18
// Bio: Used to display debug information
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;
using UnityEngine.UI;

public class DebugInformation : MonoBehaviour
{
    // Agent component variable to hold the owner of the debug information
    private Agent xOwner;
    // UI elements to hold the text information
    [SerializeField] private Text xAlertStatus;
    [SerializeField] private Text xLKPStatus;
    [SerializeField] private Text xSearchTimeStatus;
    // Float variable used to hold the timer information
    private float fTimer = 0;

    // Start runs on startup
    void Start ()
    {
        // Retrieve the components from the agent
        xOwner = GetComponent<Agent>();
        fTimer = xOwner.GetSearchTime();
	}
	
	// Update is called once per frame
	void Update ()
    {
        // If the alert level equals 10
        if(xOwner.GetAlertLevel() == 10)
        {
            // Assign the search time
            fTimer = xOwner.GetSearchTime();
        }
        // If the alert level is within bounds
        if (xOwner.GetAlertLevel() > 0 && xOwner.GetAlertLevel() < 10)
        {
            // Subtract the timer
            fTimer = fTimer - Time.deltaTime;
        }
        // If the alert level equals 0 or if the timer is below 0
        if (fTimer <= 0 || xOwner.GetAlertLevel() == 0)
        {
            // Assign the search time
            fTimer = xOwner.GetSearchTime();
        }

        // Update the UI text
        xAlertStatus.text = "Alertness: " + xOwner.GetAlertLevel();
        xLKPStatus.text = "LKP: " + xOwner.GetLKP();
        xSearchTimeStatus.text = "Timer: " + fTimer;
    }
}
