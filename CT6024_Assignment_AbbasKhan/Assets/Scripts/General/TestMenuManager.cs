﻿///////////////////////////////////////////////////////////////////
// Name: TestMenuManager.cs
// Author: Abbas Khan
// Date: 08/12/18
// Bio: Used to return to the test menu
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestMenuManager : MonoBehaviour
{
    // Function to load the menu
    public void LoadTestMenu()
    {
        // Load the main menu
        SceneManager.LoadScene(0);
    }
}
