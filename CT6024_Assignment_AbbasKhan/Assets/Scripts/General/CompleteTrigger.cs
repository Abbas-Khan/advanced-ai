﻿///////////////////////////////////////////////////////////////////
// Name: CompleteTrigger.cs
// Author: Abbas Khan
// Date: 07/10/18
// Bio: Used to control the scene completion
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;
using UnityEngine.SceneManagement;

public class CompleteTrigger : MonoBehaviour
{
    // Function that occurs on trigger enter
    private void OnTriggerEnter(Collider a_xOther)
    {
        // If the object is the spy
        if (a_xOther.name == "Spy")
        {
            // Load the main menu
            SceneManager.LoadScene(0);
        }
    }
}
