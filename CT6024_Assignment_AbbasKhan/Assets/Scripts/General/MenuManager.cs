﻿///////////////////////////////////////////////////////////////////
// Name: MenuManager.cs
// Author: Abbas Khan
// Date: 08/12/18
// Bio: Used to control scene navigation
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    // Function to load a test
    public void LoadTest(int a_SceneIndex)
    {
        // Load the scene
        SceneManager.LoadScene(a_SceneIndex);
    }
}
