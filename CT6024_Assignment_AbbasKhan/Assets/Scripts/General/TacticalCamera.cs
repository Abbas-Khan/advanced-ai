﻿///////////////////////////////////////////////////////////////////
// Name: TacticalCamera.cs
// Author: Abbas Khan
// Date: 25/09/18
// Bio: Used to control the camera
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;

public class TacticalCamera : MonoBehaviour
{
    // Rotate the camera rightwards around the map
    public void RotateRight()
    {
        transform.Rotate(Vector3.up, 90f, Space.Self);
    }

    // Rotate the camera leftwards around the map
    public void RotateLeft()
    {
        transform.Rotate(Vector3.up, -90f, Space.Self);
    }
}
