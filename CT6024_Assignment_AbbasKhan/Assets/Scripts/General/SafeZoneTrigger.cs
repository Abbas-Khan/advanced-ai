﻿///////////////////////////////////////////////////////////////////
// Name: SafeZoneTrigger.cs
// Author: Abbas Khan
// Date: 08/12/18
// Bio: Used to teleport the spy into and out of the saafe zone
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;

public class SafeZoneTrigger : MonoBehaviour
{
    // Gameobject to hold the safe zone door movement point
    [SerializeField] private GameObject xSafeZoneDoorPoint;
    // Function that occurs on trigger enter
    private void OnTriggerEnter(Collider a_xOther)
    {
        // If the object is the spy
        if (a_xOther.name == "Spy")
        {
            // Move the player to the location
            a_xOther.transform.position = xSafeZoneDoorPoint.transform.position;
        }
    }
}
