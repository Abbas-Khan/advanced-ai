﻿///////////////////////////////////////////////////////////////////
// Name: KnowledgeandEventManager.cs
// Author: Abbas Khan
// Date: 19/12/18
// Bio: Used to manage the knowledge and events in the world
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;

public class KnowledgeandEventManager : MonoBehaviour
{
    // Array to hold all the Guard agents
    private Agent[] xGuardAgents;
    // Boolean used to determine if any of the guards are on alert
    [SerializeField] private bool bAlertedGuards = false;
    // Boolean used to determine if any of the guards are searching
    [SerializeField] private bool bGuardsSearching = false;
    // String variable used to exclude the spy from the array
    [SerializeField] private string sExcludedObject;

    // Start runs on startup
    void Start ()
    {
        // Find all the guards
        GameObject[] xAgentObjects = GameObject.FindGameObjectsWithTag("Guard");
        // Setup the array
        xGuardAgents = new Agent[xAgentObjects.Length];
        // If the array is bigger than 0
        if (xAgentObjects.Length > 0)
        {
            // Loop through the amount of objects in the array
            for (int i = 0; i < xAgentObjects.Length; i++)
            {
                // Extract the agent component
                Agent xAgentComponent = xAgentObjects[i].GetComponent<Agent>();
                // Null check and not excluded check
                if (xAgentComponent != null && xAgentObjects[i].name != sExcludedObject)
                {
                    // Assign the value to the array
                    xGuardAgents[i] = xAgentObjects[i].GetComponent<Agent>();
                }
            }
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        // Reset the booleans at the end of the frame
        bGuardsSearching = false;
        bAlertedGuards = false;

        // Loop through every agent in the array
		foreach (Agent xAgent in xGuardAgents)
        {
            // If any are at alert level 10
            if(xAgent.GetAlertLevel() == 10)
            {
                // Guards are alerted
                bAlertedGuards = true;
            }
            // If any are at an alert level between 1 and 9
            if (xAgent.GetAlertLevel() > 0 && xAgent.GetAlertLevel() < 10)
            {
                // Guards are searching
                bGuardsSearching = true;
            }
        }

        // If the guards are alerted
        if(bAlertedGuards == true)
        {
            // Loop through all the agents
            foreach (Agent xAgent in xGuardAgents)
            {
                // Send a message too all guards to update their LKP values (Player Detcted Event)
                xAgent.BroadcastMessage("UpdateLKP", xAgent.GetTargetObject().transform.position, SendMessageOptions.DontRequireReceiver);
            }
        }
    }

    // Get function for the Alert boolean
    public bool GetAlertStatus()
    {
        return bAlertedGuards;
    }

    // Get function for the Search boolean
    public bool GetSearchStatus()
    {
        return bGuardsSearching;
    }
}
