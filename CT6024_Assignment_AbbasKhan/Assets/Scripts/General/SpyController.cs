﻿///////////////////////////////////////////////////////////////////
// Name: SpyController.cs
// Author: Abbas Khan
// Date: 07/10/18
// Bio: Used to control the Spy movement through input
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;

public class SpyController : MonoBehaviour
{
    // Float variables to hold the input value
    private float fxInput = 0f;
    private float fyInput = 0f;
    // Float to holf the speed of the movement
    [SerializeField] private float fSpeed = 10f;

	// Update is called once per frame
	void Update ()
    {
        // Movement input stored into the floats
        fxInput = Input.GetAxis("Horizontal");
        fyInput = Input.GetAxis("Vertical");
        // Vector3 to hold the direction
        Vector3 Direction = new Vector3(fxInput, 0, fyInput);
        // Normalise it
        Direction.Normalize();
        // Apply the movement to the object
        transform.position += Direction * Time.deltaTime * fSpeed;
    }
}
