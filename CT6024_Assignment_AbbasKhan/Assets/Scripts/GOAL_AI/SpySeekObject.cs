﻿///////////////////////////////////////////////////////////////////
// Name: SpySeekObject.cs
// Author: Abbas Khan
// Date: 19/12/18
// Bio: Class for Goal Agent Seeking behaviour
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;

public class SpySeekObject : MonoBehaviour
{
    // String used to hold the agent type this object will be hiding from
    [SerializeField] private string sAgentType = "Item";
    // Goal Agent variable used to hold the Goal agent for this object
    private GoalAgent xOwnerAgent;
    // Variable to hold a reference to the Knowledge and Event Manager within the scene
    [SerializeField] private KnowledgeandEventManager xKnowledgeandEventManager;
    // Variable used to hold the closest object with the given type
    private GoalAgent xClosestSpot = null;
    // Boolean to determine if there are enemies nearby
    bool bEnemyNearby = false;
    // Float used to calculate the distance to the seeked object
    float fDistance = Mathf.Infinity;

    // Start runs on startup
    void Start()
    {
        // Get the component on this object
        xOwnerAgent = GetComponent<GoalAgent>();
    }

    // Function that performs the behaviour for this action
    void PerformBehaviour()
    {
        // Set it false at the start of the frame because they may no longer be nearby
        bEnemyNearby = false;

        // If there are no object of that type
        if (GoalAgent.xAgents.ContainsKey(sAgentType) == false)
        {
            // return as it can't be seeked
            return;
        }

        // If the guards are not currently searching or alert
        if (xKnowledgeandEventManager.GetAlertStatus() == false && xKnowledgeandEventManager.GetSearchStatus() == false)
        {
            // Set closest to null as there may be a new closer object
            xClosestSpot = null;
            // Reset the distance
            fDistance = Mathf.Infinity;
            // Loop through all the agents in the array
            foreach (GoalAgent xAgent in GoalAgent.xAgents[sAgentType])
            {
                // Float used to calculate the distance
                float fActualDistance = Vector3.Distance(this.transform.position, xAgent.transform.position);
                // If the ckosest object is null or the distance is within bounds
                if (xClosestSpot == null || fActualDistance < fDistance)
                {
                    // Assign the object and distance
                    xClosestSpot = xAgent;
                    fDistance = fActualDistance;
                }
            }

            // If there are no hiding spots at this point
            if (xClosestSpot == null)
            {
                // return as impossible to hide
                return;
            }
        }

        // If the guards are no longer searching
        if (xKnowledgeandEventManager.GetSearchStatus() == false)
        {
            // Loop through the array
            foreach (GoalAgent xGoalAgent in GoalAgent.xAgents["Guard"])
            {
                // Calculate the distance
                float fLocalDistance = Vector3.Distance(this.transform.position, xGoalAgent.transform.position);
                // If the distance is lower than the threshold
                if (fLocalDistance < 8)
                {
                    // There is an enemy nearby
                    bEnemyNearby = true;
                }
            }
        }

        // If there is an enemy nearby
        if (bEnemyNearby == true)
        {
            // Back out as we don't want to go for the object
            return;
        }

        // If the closest object is not null
        if (xClosestSpot != null)
        {
            // Vector3 to hold the position for the weighted action
            Vector3 v3Position = xClosestSpot.transform.position;
            // Create and assign a weighted action
            WeightedAction xWeightedAction = new WeightedAction(v3Position, 1);
            // Add it to the list
            xOwnerAgent.GetDesiredActionsList().Add(xWeightedAction);
        }
    }
}
