﻿///////////////////////////////////////////////////////////////////
// Name: SpyEvade.cs
// Author: Abbas Khan
// Date: 19/12/18
// Bio: Class for Goal Agent Evasion behaviour
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;

public class SpyEvade : MonoBehaviour
{
    // String used to hold the agent type this object will be hiding from
    [SerializeField] private string sAgentType = "Guard";
    // Goal Agent variable used to hold the Goal agent for this object
    private GoalAgent xOwnerAgent;
    // Variable to hold a reference to the Knowledge and Event Manager within the scene
    [SerializeField] private KnowledgeandEventManager xKnowledgeandEventManager;

    // Start runs on startup
    void Start()
    {
        // Get the component on this object
        xOwnerAgent = GetComponent<GoalAgent>();
    }

    // Function that performs the behaviour for this action
    void PerformBehaviour()
    {
        // If the guards are not alerted
        if (xKnowledgeandEventManager.GetAlertStatus() == false)
        {
            // Return as there is no need to evade
            return;
        }

        // If the agent is hidden
        if (xOwnerAgent.GetBooleanIsHidden() == true)
        {
            // Return as we don't have to evade anything.
            return;
        }

        // If there are no object of that type
        if (GoalAgent.xAgents.ContainsKey(sAgentType) == false)
        {
            // return as it can't be evaded from
            return;
        }

        // Variable used to hold the closest object with the given type
        GoalAgent xClosestGuard = null;
        // Float used to calculate the distance to the seeked object
        float fDistance = Mathf.Infinity;

        // Loop through the array
        foreach (GoalAgent xGoalAgent in GoalAgent.xAgents[sAgentType])
        {
            // Calculate the distance
            float fLocalDistance = Vector3.Distance(this.transform.position, xGoalAgent.transform.position);
            // If the ckosest object is null or the distance is within bounds
            if (xClosestGuard == null || fLocalDistance < fDistance)
            {
                // Assign the object and distance
                xClosestGuard = xGoalAgent;
                fDistance = fLocalDistance;
            }
        }

        // If no goal agent was found
        if (xClosestGuard == null)
        {
            // return as impossible to evade
            return;
        }

        // Vector3 to hold the position for the weighted action
        Vector3 v3Position = xClosestGuard.transform.position - this.transform.position;
        // Multiply it by -1 as to avoid the Guard
        v3Position *= -1;
        // Target position is equal to this transform's position and the offset
        Vector3 TargetPosition = this.transform.position + v3Position;
        // Set the final position equal to it
        v3Position = TargetPosition;

        // Variable used to calculate te weight of the action depending on the distance
        float fWeighting = 10 / (fDistance * fDistance);
        // Create and assign a weighted action
        WeightedAction xWeightedAction = new WeightedAction(v3Position, fWeighting);
        // Add it to the list
        xOwnerAgent.GetDesiredActionsList().Add(xWeightedAction);
    }
}
