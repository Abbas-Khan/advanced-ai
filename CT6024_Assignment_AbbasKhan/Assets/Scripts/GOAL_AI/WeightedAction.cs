﻿///////////////////////////////////////////////////////////////////
// Name: WeightedAction.cs
// Author: Abbas Khan
// Date: 19/12/18
// Bio: Class for data type with weighting
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;

public class WeightedAction
{
    // Vector3 to hold the position for movement
    public Vector3 v3Position;
    // Float to hold the weight of the movement
    public float fWeight;

    // Constructor
    public WeightedAction(Vector3 a_v3Position, float a_fWeight)
    {
        // Assign the values
        v3Position = a_v3Position;
        fWeight = a_fWeight;
    }
}
