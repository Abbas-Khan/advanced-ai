﻿///////////////////////////////////////////////////////////////////
// Name: SpyHide.cs
// Author: Abbas Khan
// Date: 19/12/18
// Bio: Class for Goal Agent Hiding behaviour
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;

public class SpyHide : MonoBehaviour
{
    // String used to hold the agent type this object will be hiding from
    [SerializeField] private string sAgentType = "Guard";
    // Goal Agent variable used to hold the Goal agent for this object
    private GoalAgent xOwnerAgent;
    // Variable to hold a reference to the Knowledge and Event Manager within the scene
    [SerializeField] private KnowledgeandEventManager xKnowledgeandEventManager;
    // Variable used to hold the closest hiding spot
    [SerializeField] private GameObject xClosestHidingSpot = null;
    // Array to hold all the hiding spots within the scene
    [SerializeField] private GameObject[] xHidingSpots;

    // Start runs on startup
    void Start()
    {
        // Get the component on this object
        xOwnerAgent = GetComponent<GoalAgent>();
        // Populate the array
        xHidingSpots = GameObject.FindGameObjectsWithTag("Hiding_Spot");
    }

    // Function that performs the behaviour for this action
    void PerformBehaviour()
    {
        // If there are no guards
        if (GoalAgent.xAgents.ContainsKey(sAgentType) == false)
        {
            // There is nothing to hide from, so return
            return;
        }

        // If the agent is currently hidden and the closest object does not equal null
        if (xOwnerAgent.GetBooleanIsHidden() == true && xClosestHidingSpot != null)
        {
            // Calculate the distance to the closest object
            float fDistance = Vector3.Distance(this.transform.position, xClosestHidingSpot.transform.position);
            // If the distance is higher than the threshold
            if (fDistance > 1f)
            {
                // The agent is no longer hidden
                xOwnerAgent.SetBooleanIsHidden(false);
            }
        }

        // If the guards are currently searching
        if (xKnowledgeandEventManager.GetSearchStatus() == true)
        {
            // Variable used to determine the closest hiding spot
            float fLocalDistance = Mathf.Infinity;
            // Loop through all the objects in the array
            foreach (GameObject xSpot in xHidingSpots)
            {
                // Calculate the distance
                float fActualDistance = Vector3.Distance(this.transform.position, xSpot.transform.position);
                // If the ckosest object is null or the distance is within bounds
                if (xClosestHidingSpot == null || fActualDistance < fLocalDistance)
                {
                    // Assign the object and distance
                    xClosestHidingSpot = xSpot;
                    fLocalDistance = fActualDistance;
                }
            }

            // If there are no hiding spots at this point
            if (xClosestHidingSpot == null)
            {
                // return as impossible to hide
                return;
            }
        }

        // If search and alert are both false
        if (xKnowledgeandEventManager.GetAlertStatus() == false && xKnowledgeandEventManager.GetSearchStatus() == false)
        {
            // Nothing to fear, so no need to hide
            return;
        }

        // Vector3 to hold the position for the weighted action
        Vector3 v3Position = xClosestHidingSpot.transform.position;
        // Create and assign a weighted action
        WeightedAction xWeightedAction = new WeightedAction(v3Position, 9);
        // Add it to the list
        xOwnerAgent.GetDesiredActionsList().Add(xWeightedAction);
    }
}
