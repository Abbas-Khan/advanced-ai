﻿///////////////////////////////////////////////////////////////////
// Name: GoalAgent.cs
// Author: Abbas Khan
// Date: 19/12/18
// Bio: Class for every Goal-oriented object
///////////////////////////////////////////////////////////////////

// Using Calls
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GoalAgent : MonoBehaviour
{
    // Public static variable to serve as the data contaimer for all the Goal-Oriented Behaviour systems
    public static Dictionary<string, List<GoalAgent>> xAgents;
    // List used to hold all the actions from the behaviours
    private List<WeightedAction> xDesiredActions;
    // Boolean used to determine if the object is hidden
    private bool bIsHidden = false;
    // Boolean used to determine if the object is the spy
    [SerializeField] private bool bIsSpy = false;
    // Boolean used to determine if the object is an enemy
    [SerializeField] private bool bIsEnemy = false;
    // Boolean used to add an enemy to the dictionary
    private bool bAddEnemy = false;
    // String used to hold the type for this agent
    [SerializeField] private string sAgentType;
    // Vector3 used to hold the target position of the spy
    private Vector3 v3TargetPosition;
    // Vector3 used to assist in destination setu
    private Vector3 v3Spot = Vector3.zero;
    // Variable used to hold the nav mesh agent of the spy
    private NavMeshAgent xNavMeshAgent;
    // Variable used to hold the layer of the enemy
    [SerializeField] private LayerMask xEnemyLayer;
    // Floats used to determine the vision sense fo the goal agent
    [SerializeField] private float fProximityRange = 5f;
    [SerializeField] private float fVisionRange = 20f;
    // Boolean used to determine if the guard agent was added to the dictionary
    private bool bAdded = false;

    // Utilty function used to show debug info
    private void OnDrawGizmosSelected()
    {
        // If it's the spy
        if(bIsSpy == true)
        {
            Gizmos.color = Color.blue;
            // Sphere for the proximity detection
            Gizmos.DrawWireSphere(transform.position, fProximityRange);
            // Rays for the vision radius and max distance
            Gizmos.DrawRay(transform.position, transform.forward.normalized * fVisionRange);
            Gizmos.DrawRay(transform.position, (transform.forward + transform.right).normalized * fVisionRange);
            Gizmos.DrawRay(transform.position, (transform.forward - transform.right).normalized * fVisionRange);
        }
    }

    // Start runs on startup
    void Start()
    {
        // If this object is the spy
        if(bIsSpy == true)
        {
            // Extract the nav mesh agent
            xNavMeshAgent = GetComponent<NavMeshAgent>();
        }

        // If the dictionary is null
        if (xAgents == null)
        {
            // Create a new one
            xAgents = new Dictionary<string, List<GoalAgent>>();
        }

        // If the agent does not contain this agenttype
        if (xAgents.ContainsKey(sAgentType) == false)
        {
            // Add the agent type to the dictionary
            xAgents[sAgentType] = new List<GoalAgent>();
        }

        // If this is not the enemy
        if (bIsEnemy == false)
        {
            // Automatically add yourself to the dictionary
            xAgents[sAgentType].Add(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // If the object is a Guard and hasn't been added to the dictionary
        if (bIsEnemy == true && bAddEnemy == true && bAdded == false)
        {
            // Add them
            xAgents[sAgentType].Add(this);
            // Set added to true to prevent repeat additions
            bAdded = true;
        }

        // If this object is the spy
        if (bIsSpy == true)
        {
            // If they are hidden
            if (bIsHidden == true)
            {
                // Turn off the capsule collider to prevent detection
                GetComponent<CapsuleCollider>().enabled = false;
            }
            // Otherwise
            else if (bIsHidden == false)
            {
                // Turn on the capsule collider
                GetComponent<CapsuleCollider>().enabled = true;
            }

            // If the spy sees a guard through proximity or vision
            if(CheckDetectionByProximity() == true || CheckDetectionByVision() == true)
            {
                // Start the knowledge wipe
                StartCoroutine(SpyKnowledgeWipe());
            }

            // Assign the weighted actions list
            xDesiredActions = new List<WeightedAction>();
            // Send out a broadcast to perform the behaviour on other behaviour scripts
            BroadcastMessage("PerformBehaviour", SendMessageOptions.DontRequireReceiver);

            // Vector3 used to hold the potential target position for the spy to move towards
            Vector3 v3PotentialTargetPosition = Vector3.zero;
            // // Float to be used in determining which action to perform
            float fWeightingThreshold = 0;

            // Loop through the array
            foreach (WeightedAction xWeightedAction in xDesiredActions)
            {
                // Get the weight of the action
                float fActualWeight = xWeightedAction.fWeight;
                // If the vector3 equals zero (has not been assigned) or the weighting exceeds the threshold
                if (v3Spot == Vector3.zero || fActualWeight > fWeightingThreshold)
                {
                    // Assign he values
                    v3Spot = xWeightedAction.v3Position;
                    fWeightingThreshold = fActualWeight;
                }
            }

            // Set Potential position equal to it 
            v3PotentialTargetPosition = v3Spot;
            // Set the target position equal to it
            v3TargetPosition = v3PotentialTargetPosition;
            // Set the destination on the nav mesh agent
            xNavMeshAgent.SetDestination(v3TargetPosition);
        }
    }

    // Destroy is called when this object is being destroyed
    void OnDestroy()
    {
        // Remove it from the static dictionary
        xAgents[sAgentType].Remove(this);
    }

    // On trigger enter calls when an object enters the trigger area for this object
    void OnTriggerEnter(Collider other)
    {
        // If the other object is called hiding spot
        if (other.name == "Hiding_Spot")
        {
            // Then set hidden to true
            bIsHidden = true;
        }
    }

    // Boolean for checking if the target is close
    bool CheckDetectionByProximity()
    {
        // Array of colliders that is filled with the objects found by the overlap sphere on the target layer
        Collider[] xHitColliders = Physics.OverlapSphere(transform.position, fProximityRange, xEnemyLayer);
        // If the array is bigger than 0
        if (xHitColliders.Length > 0)
        {
            // Loop through the array
            foreach (Collider xCollider in xHitColliders)
            {
                // Goal agent variable used to hold a reference to the other object's version of the script
                GoalAgent xGoalAgent = null;
                // If it's a guard
                if (xCollider.transform.tag == "Guard")
                {
                    // Extract the component
                    xGoalAgent = xCollider.transform.gameObject.GetComponent<GoalAgent>();
                }
                // If the component is not null
                if (xGoalAgent != null)
                {
                    // Set the addenemy boolean to true which adds the guard to the dictionary
                    xGoalAgent.bAddEnemy = true;
                }
            }
            // Return true
            return true;
        }
        else
        {
            // Return false
            return false;
        }
    }

    // Boolean for checking if the target is seen
    bool CheckDetectionByVision()
    {
        // Create a raycast hit
        RaycastHit xHit;

        // Directions for the target direction
        Vector3 v3TargetDirection = (transform.forward.normalized * fVisionRange);

        // If the raycast hits anything
        if (Physics.SphereCast(transform.position, 10f, v3TargetDirection, out xHit, fVisionRange))
        {
            // Goal agent variable used to hold a reference to the other object's version of the script
            GoalAgent xGoalAgent = null;
            // If it's a guard
            if (xHit.transform.tag == "Guard")
            {
                // Extract the component
                xGoalAgent = xHit.transform.gameObject.GetComponent<GoalAgent>();
            }
            // If the component is not null
            if (xGoalAgent != null)
            {
                // Set the addenemy boolean to true which adds the guard to the dictionary
                xGoalAgent.bAddEnemy = true;
            }
            // Return true
            return true;
        }
        // Return false
        return false;
    }

    // Get function for the nav mesh agent
    public NavMeshAgent GetNavMeshAgent()
    {
        return xNavMeshAgent;
    }

    // Get function for the ishidden boolean
    public bool GetBooleanIsHidden()
    {
        return bIsHidden;
    }

    // Set function for the ishidden boolean
    public void SetBooleanIsHidden(bool a_NewValue)
    {
        bIsHidden = a_NewValue;
    }

    // Get function for the Desired actions list 
    public List<WeightedAction> GetDesiredActionsList()
    {
        return xDesiredActions;
    }

    // Ienumerator used to wipe the spy's knowledge after an amount of time
    private IEnumerator SpyKnowledgeWipe()
    {
        yield return new WaitForSeconds(120);
        // Remove the guards from the dictionary
        xAgents["Guard"].Clear();
    }
}
