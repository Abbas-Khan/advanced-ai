﻿///////////////////////////////////////////////////////////////////
// Name: Parallel.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used to run through a selection of nodes simultaneously inherits from composite
///////////////////////////////////////////////////////////////////

public class Parallel : Composite
{
    // Boolean used to determine if the parallel fails if one node fails
    private bool bFailOnAll = false;
    // Integer used to count the number of failed nodes
    private int fFailedNodes = 0;

    // Constructor
    public Parallel(bool a_bFailOnAll)
    {
        // Call init to setup the node list and current index
        Init();
        // Assign the value
        bFailOnAll = a_bFailOnAll;
    }

    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        // Behaviour enum that by default is success
        BEHAVIOUR_STATUS eReturnStatus = BEHAVIOUR_STATUS.SUCCESS;

        // Loop through all the nodes
        for (int i = 0; i < GetChildBehaviours().Count - 1; i++)
        {
            // Node variable to get the node next in the parallel
            Node xCurrentBehaviour = GetChildBehaviours()[i];
            // If the behaviour is not null (Safe Guard)
            if (xCurrentBehaviour != null)
            {
                // Update the behaviour ans store the return value into an enumeration
                BEHAVIOUR_STATUS eBehaviourStatus = xCurrentBehaviour.Update();
                // If it failed
                if (eBehaviourStatus == BEHAVIOUR_STATUS.FAILURE)
                {
                    // Increment the counter
                    fFailedNodes = fFailedNodes + 1;
                }
                // If it is still running
                if (eBehaviourStatus == BEHAVIOUR_STATUS.RUNNING)
                {
                    // Set the return enumeration to running as a node is still running
                    eReturnStatus = BEHAVIOUR_STATUS.RUNNING;
                }
            }
        }

        // If fail all is false
        if (bFailOnAll == false)
        {
            // Check if the counter is greater than 0
            if(fFailedNodes > 0)
            {
                // Set the return enumeration to failure as one node has failed
                eReturnStatus = BEHAVIOUR_STATUS.FAILURE;
            }
        }
        // If fail all is true
        else if (bFailOnAll == true)
        {
            // Check if the counter is equal to the size of the parallel
            if (fFailedNodes == GetChildBehaviours().Count - 1)
            {
                // Set the return enumeration to failure as all nodes have failed
                eReturnStatus = BEHAVIOUR_STATUS.FAILURE;
            }
        }

        // Safe Guard check
        if (eReturnStatus == BEHAVIOUR_STATUS.SUCCESS || eReturnStatus == BEHAVIOUR_STATUS.FAILURE)
        {
            // Reset the index and amount of failed nodes for the next loop
            Reset();
            fFailedNodes = 0;
        }

        // Return the behaviour status enumeration created at the top of this update loop with its value of success or failure if changed
        return eReturnStatus;
    }
}
