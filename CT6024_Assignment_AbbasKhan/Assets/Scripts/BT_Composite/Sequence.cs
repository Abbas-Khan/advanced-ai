﻿///////////////////////////////////////////////////////////////////
// Name: Sequence.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used to run through a sequence of nodes inherits from composite
///////////////////////////////////////////////////////////////////

public class Sequence : Composite
{
    // Constructor
    public Sequence ()
    {
        // Call init to setup the node list and current index
        Init();
    }

    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        // Behaviour enum that by default is failure
        BEHAVIOUR_STATUS eReturnStatus = BEHAVIOUR_STATUS.FAILURE;
        // Node variable to get the node next in the sequence
        Node xCurrentBehaviour = GetChildBehaviours()[iCurrentIndex];
        // Null check safe guard
        if (xCurrentBehaviour != null)
        {
            // Update the behaviour ans store the return value into an enumeration
            BEHAVIOUR_STATUS eBehaviourStatus = xCurrentBehaviour.Update();
            // If it's success
            if (eBehaviourStatus == BEHAVIOUR_STATUS.SUCCESS)
            {
                // If it's at the last element
                if (iCurrentIndex == GetChildBehaviours().Count -1)
                {
                    // Set the enumeration to success
                    eReturnStatus = BEHAVIOUR_STATUS.SUCCESS;
                }
                else
                {
                    // Otherwise increment the index
                    iCurrentIndex = iCurrentIndex + 1;
                    // Set the enumeration to running
                    eReturnStatus = BEHAVIOUR_STATUS.RUNNING;
                }
            }
            else
            {
                // Otherwise if it's not success set the return enumeration equal to it
                eReturnStatus = eBehaviourStatus;
            }
        }

        // Safe Guard check
        if (eReturnStatus == BEHAVIOUR_STATUS.SUCCESS || eReturnStatus == BEHAVIOUR_STATUS.FAILURE)
        {
            // Reset the index
            Reset();
        }

        // Return the behaviour status enumeration created at the top of this update loop with its value of success or failure if changed
        return eReturnStatus;
    }
}
