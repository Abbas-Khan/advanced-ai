﻿///////////////////////////////////////////////////////////////////
// Name: FindNextPatrolPoint.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used to find the next patrol position inherits from action
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;

public class FindNextPatrolPoint : Action
{
    // float used to determine an acceptable distance to the target location
    private float fDistanceToTarget = 1f;
    // Index used to step through the patrol points array in Agent.cs
    private int iIndex = 0;

    // Constructor
    public FindNextPatrolPoint(Agent a_xOwnerAgent, float a_fAcceptableDistance) : base(a_xOwnerAgent)
    {
        // Assign the passed in value
        fDistanceToTarget = a_fAcceptableDistance;
    }
    
    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        // Bounds safe guard for the index
        if(iIndex > GetOwner().GetPatrolPoints().Length - 1)
        {
            // Reset if it exceeds the bounds
            iIndex = 0;
        }

        // Local variable used to store the target position
        Vector3 v3Target = GetOwner().GetPatrolPoints()[iIndex].transform.position;

        // Local float to calculate the distance between the agent and its target
        float fNewDistance = Vector3.Distance(GetOwner().GetTransform().position, v3Target);

        // If it is within an acceptable distance
        if (fNewDistance < fDistanceToTarget)
        {
            // Bounds safe guard
            if(iIndex < GetOwner().GetPatrolPoints().Length)
            {
                // Increment the index
                iIndex = iIndex + 1;
            }
        }

        // If the target does not equal zero (Safe Guard)
        if (v3Target != Vector3.zero)
        {
            // Set the target position and return success
            GetOwner().SetTargetPosition(v3Target);
            return BEHAVIOUR_STATUS.SUCCESS;
        }
        else
        {
            // Otherwise return running
            return BEHAVIOUR_STATUS.RUNNING;
        }
    }
}
