﻿///////////////////////////////////////////////////////////////////
// Name: Wait.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used to Wait inherits from action
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;

public class Wait : Action
{
    // Float to hold the amount of time waited
    private float fOriginalWaitTime = 0.5f;
    // Float to use for the actual timing
    private float fWaitTimer = 0;

    // Constructor
    public Wait(Agent a_xOwnerAgent, float a_WaitTime) : base(a_xOwnerAgent)
    {
        // Assign the passed in value
        fOriginalWaitTime = a_WaitTime;
        // Setup the timer
        fWaitTimer = fOriginalWaitTime;
    }

    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        // If the timer is less than 0
        if((fWaitTimer -= Time.deltaTime) < 0)
        {
            // Set the colour of the agent to black to indicate that it's no longer waiting
            GetOwner().SetColour(Color.black);
            // Reset the timer
            fWaitTimer = fOriginalWaitTime;
            // Return success
            return BEHAVIOUR_STATUS.SUCCESS;
        }
        else
        {
            // Otherwise Set the colour of the agent to yellow to indicate that it's waiting
            GetOwner().SetColour(Color.yellow);
            // Return running
            return BEHAVIOUR_STATUS.RUNNING;
        }
    }
}
