﻿///////////////////////////////////////////////////////////////////
// Name: FindRandomPosition.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used to find a random position inherits from action
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;
using UnityEngine.AI;

public class FindRandomPosition : Action
{
    // Float to hold the area for searching
    private float fSearchRadius = 5f;

    // Constructor
    public FindRandomPosition(Agent a_xOwnerAgent, float a_fSearchRadius) : base(a_xOwnerAgent)
    {
        // Assign the passed in value
        fSearchRadius = a_fSearchRadius;
    }

    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        // Local variable used to a random position relative to the LKP (5 is the radius of the sphere)
        Vector3 v3RandomPoint = GetOwner().GetLKP() + (Random.insideUnitSphere * fSearchRadius);
        // NavMeshHit variable used to determine if the point is on the nav mesh
        NavMeshHit xHit;
        // Variable used to hold the new target location for the AI
        Vector3 v3NewTarget = Vector3.zero;

        // If the position is on the nav mesh
        if (NavMesh.SamplePosition(v3RandomPoint, out xHit, 5f, GetOwner().GetNavMeshAgent().areaMask))
        {
            // Set the new target location
            v3NewTarget = v3RandomPoint;
        }

        // If the new target location does not equal zero (Safe Guard)
        if (v3NewTarget != Vector3.zero)
        {
            // Set the target position for the agent
            GetOwner().SetTargetPosition(v3NewTarget);
            return BEHAVIOUR_STATUS.SUCCESS;
        }
        else
        {
            // Otherwise tell the agent to stop and return running
            GetOwner().GetNavMeshAgent().isStopped = true;
            return BEHAVIOUR_STATUS.RUNNING;
        }
    }
}
