﻿///////////////////////////////////////////////////////////////////
// Name: FindSpyPosition.cs
// Author: Abbas Khan
// Date: 23/10/18
// Bio: Used to find the spy position inherits from action
///////////////////////////////////////////////////////////////////

// Using Calls
using UnityEngine;

public class FindSpyPosition : Action
{
    // Constructor
    public FindSpyPosition(Agent a_xOwnerAgent) : base(a_xOwnerAgent)
    {
    }

    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        // If the target does not equal zero (Safe Guard)
        if (GetOwner().GetTargetObject().transform.position != Vector3.zero)
        {
            // Set the target position and return success
            GetOwner().SetTargetPosition(GetOwner().GetTargetObject().transform.position);
            return BEHAVIOUR_STATUS.SUCCESS;
        }
        else
        {
            // Otherwise return running
            return BEHAVIOUR_STATUS.RUNNING;
        }
    }
}
