﻿///////////////////////////////////////////////////////////////////
// Name: MoveToTarget.cs
// Author: Abbas Khan
// Date: 24/10/18
// Bio: Used to move agents inherits from action
///////////////////////////////////////////////////////////////////

// Using calls
using UnityEngine;

public class MoveToTarget : Action
{
    // Constructor
    public MoveToTarget(Agent a_xOwnerAgent) : base(a_xOwnerAgent)
    {
    }

    // Update loop for this script
    public override BEHAVIOUR_STATUS Update()
    {
        // If the target does not equal zero (Safe Guard)
        if (GetOwner().GetTargetPosition() != Vector3.zero)
        {
            // Set stopped to false
            GetOwner().GetNavMeshAgent().isStopped = false;
            // Set the destination
            GetOwner().GetNavMeshAgent().SetDestination(GetOwner().GetTargetPosition());
            // Return success
            return BEHAVIOUR_STATUS.SUCCESS;
        }
        // Return failure if the target does equal zero
        return BEHAVIOUR_STATUS.FAILURE;
    }
}
